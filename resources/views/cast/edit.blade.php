@extends('layout.master')

@section('title')
    Halaman Cast
@endsection

@section('subtitle')
    Mengedit Cast dengan id {{$cast->id}}
@endsection

@section('body')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="body" placeholder="Masukkan Body">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">bio</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="body" placeholder="Masukkan Body">
                @error('bio_cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">edit</button>
        </form>
@endsection