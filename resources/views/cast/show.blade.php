@extends('layout.master')

@section('title')
    Halaman Cast
@endsection

@section('subtitle')
    Detail Cast dengan id {{$cast->id}}
@endsection

@section('body')

<h4>Nama : {{$cast->nama}}</h4>
<h4>Umur : {{$cast->umur}}</h4>
<h4>Bio : {{$cast->bio}}</h4>

@endsection