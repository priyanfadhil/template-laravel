@extends('layout.master')

@section('title')
    Halaman Cast
@endsection

@section('subtitle')
    Membuat Cast
@endsection

@section('body')
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">umur</label>
                <input type="text" class="form-control" name="umur" id="body" placeholder="Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">bio</label>
                <input type="text" class="form-control" name="bio" id="body" placeholder="Bio Cast">
                @error('bio_cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection